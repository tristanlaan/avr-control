from pathlib import Path
from werkzeug.exceptions import HTTPException
from flask import Flask, abort, jsonify, request, Response
from denonavr import DenonAVR
from dotenv import dotenv_values

app = Flask(__name__)

secrets = dotenv_values(Path(__file__).resolve().parent / '.env')
route = secrets.get('route', '/')

@app.errorhandler(Exception)
def handle_error(e: Exception) -> Response:
    if issubclass(type(e), HTTPException):
        description = e.description
        if e.code == 404:
            description += f" (route = {request.url})"
        response = app.make_response(jsonify({
            "errorcode": e.code,
            "name": e.name,
            "description": description,
        }))
        response.status = e.code
        response.content_type = "application/json"
        return response
    else:
        response = app.make_response(jsonify({
            "errorcode": 500,
            "name": "Internal Server Error",
            "description": f"{type(e).__name__}: {e}",
        }))
        response.status = 500
        response.content_type = "application/json"
        return response


async def handle_commands(denon: DenonAVR, commands: dict) -> None:
    if (power := commands.get('power', None)) is not None:
        if power:
            app.logger.info("Turning power on")
            await denon.async_power_on()
        else:
            app.logger.info("Turning power off")
            await denon.async_power_off()
    if (volume := commands.get('volume', None)) is not None:
        app.logger.info(f"Setting volume to {volume}")
        await denon.async_set_volume(volume)
    if (input := commands.get('input', None)) is not None:
        app.logger.info(f"Setting input to {input}")
        await denon.async_update()
        await denon.async_set_input_func(input)


async def get_status(denon: DenonAVR) -> dict:
    await denon.async_update()
    return {'name': denon.name, 'host': denon.host, 'power': denon.power,
            'input_func': denon.input_func,
            'input_func_list': denon.input_func_list, 'muted': denon.muted,
            'volume': denon.volume, 'sound_mode': denon.sound_mode,
            'sound_mode_list': denon.sound_mode_list,
            'telnet': denon.telnet_connected}


def verify_password(token: str):
    return token == secrets['token']


@app.route(route, methods=['GET', 'POST'])
async def denon() -> Response:
    if not 'token' in request.args or not verify_password(
            request.args['token']):
        abort(403)

    denon = DenonAVR("denon.famlaan.nl")

    setup = denon.async_setup()

    if request.method == 'POST':
        data = request.get_json()
    else:
        data = None

    app.logger.info(f"Request from '{request.remote_addr}' with "
                    f"options: '{data}'")

    await setup

    if data is not None:
        await handle_commands(denon, data)

    status = await get_status(denon)
    response = app.make_response(jsonify({'errorcode': 200} | status))
    response.content_type = 'application/json'
    return response
