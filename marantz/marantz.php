<?php
/**
 * Copyright (c) Tristan Laan 2020.
 * Marantz API
 *
 * This file incorporates work covered by the following copyright and
 * permission notice:
 *     https://github.com/ntotten/marantz-avr
 *     Copyright (c) 2015 Nathan Totten
 *
 *     Permission is hereby granted, free of charge, to any person obtaining a
 *     copy of this software and associated documentation files (the
 *     "Software"), to deal in the Software without restriction, including
 *     without limitation the rights to use, copy, modify, merge, publish,
 *     distribute, sublicense, and/or sell copies of the Software, and to permit
 *     persons to whom the Software is furnished to do so, subject to the
 *     following conditions:
 *
 *     The above copyright notice and this permission notice shall be included
 *     in all copies or substantial portions of the Software.
 *
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *     OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *     IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 *     CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *     TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *     SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

require_once 'BetterEnum.php';

class Sources extends BetterEnum {
    public const __default = self::UNKNOWN;

    public const CBL_SAT = 'SAT/CBL';
    public const CD = 'CD';
    public const DVD = 'DVD';
    public const BLURAY = 'BD';
    public const TUNER = 'TUNER';
    public const AUX1 = 'AUX1';
    public const AUX2 = 'AUX2';
    public const MEDIA_PLAYER = 'MPLAY';
    public const GAME = 'GAME';
    public const TV = 'TV';
    public const PHONO = 'PHONO';
    public const USB = 'USB/IPOD';
    public const NETWORK = 'NET';
    public const INTERNET_RADIO = 'IRADIO';
    public const SERVER = 'SERVER';
    public const FAVORITES = 'FAVORITES';
    public const BLUETOOTH = 'BLUETOOTH';

    public const UNKNOWN = 'UNKNOWN';

    private const MAPPING = [
        'Ziggo' => self::CBL_SAT,
        'WII' => self::DVD,
        'Blu-ray' => self::BLURAY,
        'Steam' => self::GAME,
        'AUX 1' => self::AUX1,
        'AUX 2' => self::AUX1,
        'Apple TV' => self::MEDIA_PLAYER,
        'iPod/USB' => self::USB,
        'Pi' => self::CD,
        'Tuner' => self::TUNER,
        'Online Music' => self::NETWORK,
        'TV' => self::TV,
        'Bluetooth' => self::BLUETOOTH
    ];

   public static function map(string $source) {
        return new Sources(self::MAPPING[$source] ?? SELF::UNKNOWN);
   }
}

class SurroundModes extends BetterEnum {
    public const __default = self::UNKNOWN;

    public const MOVIE = 'MOVIE';
    public const MUSIC = 'MUSIC';
    public const GAME = 'GAME';
    public const PURE_DIRECT = 'PURE DIRECT';
    public const DIRECT = 'DIRECT';
    public const STEREO = 'STEREO';
    public const STANDARD = 'STANDARD';
    public const SIMULATION = 'SIMULATION';
    public const AUTO = 'AUTO';
    public const LEFT = 'LEFT';

    public const UNKNOWN = 'UNKNOWN';

    public static function map(string $mode) {
        if (self::isValidValue($mode)) {
            return new SurroundModes($mode);
        }

        return new SurroundModes(SELF::UNKNOWN);
   }
}

class Zone implements JsonSerializable {
    var $name;
    var $power;
    var $source;
    var $surround;
    var $volume;
    var $mute;

    public function __construct(string $name, bool $power, Sources $source,
                                SurroundModes $surround, float $volume,
                                bool $mute) {
        $this->name = $name;
        $this->power = $power;
        $this->source = $source;
        $this->surround = $surround;
        $this->volume = $volume;
        $this->mute = $mute;
    }

    public static function parse_string(string $name, string $power, string $source,
                                        string $surround, string $volume,
                                        string $mute) {
        $bool_power = strtoupper($power) === "ON";
        $type_source = Sources::map($source);
        $type_surround = SurroundModes::map(strtoupper($surround));
        $bool_mute = strtoupper($mute) === "ON";

        return new Zone($name, $bool_power, $type_source, $type_surround,
                        (float) $volume, $bool_mute);
    }

    public function jsonSerialize() {
        return [
            'name' => $this->name,
            'power' => $this->power,
            'source' => $this->source,
            'surround' => $this->surround,
            'volume' => $this->volume,
            'mute' => $this->mute
        ];
    }
}

class Status implements JsonSerializable {
    var $name;
    var $power;
    var $zones;

    public function __construct(string $name, bool $power, array $zones) {
        $this->name = $name;
        $this->power = $power;
        $this->zones = $zones;
    }

    private static function download_xml(string $url) {
        $handle = fopen($url, "rb");

        if (FALSE === $handle) {
            return NULL;
        }

        $contents = '';

        while (!feof($handle)) {
            $contents .= fread($handle, 8192);
        }

        fclose($handle);

        return $contents;
    }

    private static function check_zone_tags(array $index) {
        if (!isset($index['ZONEPOWER']) && !array_key_exists('ZONEPOWER', $index)) {
            return false;
        }

        if (!isset($index['RENAMEZONE']) && !array_key_exists('RENAMEZONE', $index)) {
            return false;
        }

        if (!isset($index['INPUTFUNCSELECT']) && !array_key_exists('INPUTFUNCSELECT', $index)) {
            return false;
        }

        if (!isset($index['SELECTSURROUND']) && !array_key_exists('SELECTSURROUND', $index)) {
            return false;
        }

        if (!isset($index['MASTERVOLUME']) && !array_key_exists('MASTERVOLUME', $index)) {
            return false;
        }

        if (!isset($index['MUTE']) && !array_key_exists('MUTE', $index)) {
            return false;
        }

        return true;
    }

    private static function check_tags(array $main_index, array $sec_index) {
        if (!isset($main_index['FRIENDLYNAME']) && !array_key_exists('FRIENDLYNAME', $main_index)) {
            return false;
        }

        if (!isset($main_index['POWER']) && !array_key_exists('POWER', $main_index)) {
            return false;
        }

        if (!self::check_zone_tags($main_index)) {
            return false;
        }

        if (!self::check_zone_tags($sec_index)) {
            return false;
        }

        return true;
    }

    private static function parse_xml(string $url) {
        $contents = self::download_xml($url);

        if ($contents === NULL) {
            return [-1];
        }

        $p = xml_parser_create();

        if (0 === xml_parse_into_struct($p, $contents, $vals, $index)) {
            return [-2];
        }

        xml_parser_free($p);

        return [0, $index, $vals];
    }

    private static function construct_source($index, $vals) {
        $name = $vals[$index['RENAMEZONE'][0] + 1]['value'];
        $power = $vals[$index['ZONEPOWER'][0] + 1]['value'];
        $source = $vals[$index['INPUTFUNCSELECT'][0] + 1]['value'];
        $surround = $vals[$index['SELECTSURROUND'][0] + 1]['value'];
        $volume = $vals[$index['MASTERVOLUME'][0] + 1]['value'];
        $mute = $vals[$index['MUTE'][0] + 1]['value'];

        return Zone::parse_string(trim($name), $power, $source, trim($surround),
                                  $volume, $mute);
    }

    public static function download_status(string $ip) {
        $main_zone = 'http://' . $ip . '/goform/formMainZone_MainZoneXml.xml';
        $sec_zone = $main_zone . '?ZoneName=ZONE2';

        $return = self::parse_xml($main_zone);

        if ($return[0]) {
            return [$return[0]];
        }

        $main_index = $return[1];
        $main_vals = $return[2];

        $return = self::parse_xml($sec_zone);

        if ($return[0]) {
            return [$return[0]];
        }

        $sec_index = $return[1];
        $sec_vals = $return[2];

        if (!self::check_tags($main_index, $sec_index)) {
            return [-3];
        }

        $name = $main_vals[$main_index['FRIENDLYNAME'][0] + 1]['value'];
        $power = $main_vals[$main_index['POWER'][0] + 1]['value'];
        $bool_power = strtoupper($power) === "ON";

        $main = self::construct_source($main_index, $main_vals);
        $sec = self::construct_source($sec_index, $sec_vals);

        return [0, new Status(trim($name), $bool_power, [$main, $sec])];
    }

    public function jsonSerialize() {
        return [
            'name' => $this->name,
            'power' => $this->power,
            'zones' => $this->zones
        ];
    }
}

function is_sequential(array $arr) {
    return array_keys($arr) === range(0, count($arr) - 1);
}

function send_zone_commands($url, $commands, $timeout=2) {
    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => $commands,
            'ignore_errors' => true,
            'timeout' => 10
        )
    );

    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $commands);
    curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);

    $result = curl_exec($ch);
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if (curl_errno($ch)) {
        return ["errno" => curl_errno($ch), "command" => $commands, "message" => curl_error($ch)];
    }

    curl_close($ch);

    if ($status != 200) {
        return ["errno" => 1, "status" => $status, "command" => $commands, "message" => "unexpected response status: {$status}", "response" => $result];
    }

    return ["errno" => 0, "status" => $status, "command" => $commands, "response" => $result];
}

function send_commands($ip, $main_commands, $sec_commands) {
    $url = 'http://' . $ip . '/MainZone/index.put.asp';

    $main = http_build_query($main_commands);
    $sec = http_build_query($sec_commands);

    $output = [];

    if (isset($main_commands['cmd0'])) {
        $output["Main Zone"] = send_zone_commands($url, $main);
    }

    if (isset($main_commands['cmd0'])) {
        $output["Secondary Zone"] = send_zone_commands($url, $sec);
    }

    return $output;
}

function handle_power($command, $zone_default=0) {
    $status = (bool) $command[1];

    if (isset($command[2])) {
        $zone = (int) $command[2];

        if ($zone < 0 || $zone > 2) {
            return [1, "Invalid zone"];
        }
    } else {
        if ($status) {
            $zone = $zone_default;
        } else {
            $zone = 2;
        }
    }

    $cmd = 'PutZone_OnOff/';

    if ($status) {
        $cmd .= 'ON';
    } else {
        $cmd .= 'OFF';
    }

    return [0, $cmd, $zone];
}

function handle_mute($command, $zone_default=0) {
    $status = (bool) $command[1];

    if (isset($command[2])) {
        $zone = (int) $command[2];

        if ($zone < 0 || $zone > 2) {
            return [1, "Invalid zone"];
        }
    } else {
        if ($status) {
            $zone = $zone_default;
        } else {
            $zone = 2;
        }
    }

    $cmd = 'PutVolumeMute/';

    if ($status) {
        $cmd .= 'on';
    } else {
        $cmd .= 'off';
    }

    return [0, $cmd, $zone];
}

function handle_source($command, $zone_default=0) {
    $source = strtoupper($command[1]);

    if (!Sources::isValidValue($source) || $source === 'UNKNOWN') {
        return [1, "Invalid source name"];
    }

    $source = new Sources($source);

    if (isset($command[2])) {
        $zone = (int) $command[2];

        if ($zone < 0 || $zone > 2) {
            return [1, "Invalid zone"];
        }
    } else {
        $zone = $zone_default;
    }

    $cmd = 'PutZone_InputFunction/' . $source;

    return [0, $cmd, $zone];
}

function handle_volume($command, $zone_default=0) {
    $level = (float) $command[1];

    if (isset($command[2])) {
        $relative_mode = (bool) $command[2];

        if ($relative_mode) {
            $level -= 80;
        }
    } else {
        $level -= 80;
    }

    if ($level < -80 || $level > 18) {
        return [1, "Invalid volume level"];
    }

    if (isset($command[3])) {
        $zone = (int) $command[3];

        if ($zone < 0 || $zone > 2) {
            return [1, "Invalid zone"];
        }
    } else {
        $zone = $zone_default;
    }

    $cmd = 'PutMasterVolumeSet/' . $level;

    return [0, $cmd, $zone];
}

function handle_command($command) {
    if (!is_array($command) || !is_sequential($command) || count($command) < 2) {
        return [-1, "Command must be a sequantial array with at least 2 items"];
    }
    switch (strtolower(trim($command[0]))) {
        case 'power':
            return handle_power($command);
        case 'mute':
            return handle_mute($command);
        case 'volume':
            return handle_volume($command);
        case 'source':
            return handle_source($command);
    }
}

$correct_token = '<FILL IN TOKEN>';
$ip = '<FILL IN IP>';

if (!isset($_POST) || !isset($_POST['token'])) {
    http_response_code(403);
    exit();
}

if (!password_verify($_POST['token'], $correct_token)) {
    http_response_code(403);
    exit();
}

if (isset($_POST['commands']) && !empty($_POST['commands'])) {
    $response['input'] = ['errno' => 0];
    try {
        $input = json_decode($_POST['commands'], true, 512, JSON_THROW_ON_ERROR);
        $commands = $input['commands'];
        $error = false;

        $main_count = 0;
        $sec_count = 0;

        $main_commands = [];
        $sec_commands = ['ZoneName' => 'ZONE2'];

        for ($i = 0; $i < count($commands); $i++) {
            $command = $commands[$i];
            $response['input']['cmd' . $i] = ['errno' => 0];
            $output = handle_command($command);
            if ($output[0]) {
                $response['input']['cmd' . $i]['errno'] = $output[0];
                $response['input']['cmd' . $i]['message'] = $output[1];
                $error = true;
            } else {
                $cmd = $output[1];
                $zone = $output[2];

                if ($zone == 0 || $zone == 2) {
                    $main_commands['cmd' . $main_count++] = $cmd;
                }

                if ($zone == 1 || $zone == 2) {
                    $sec_commands['cmd' . $sec_count++] = $cmd;
                }
            }
        }

        if ($error) {
            $response['input']['errno'] = 1;
            $response['input']['message'] = "One of the commands had incorrect input";
        }

    } catch(Exception $e) {
        $response['input']['errno'] = $e->getCode();
        $response['input']['message'] = $e->getMessage();
        $main_commands = [];
        $sec_commands = [];
    }

    $response['output'] = send_commands($ip, $main_commands, $sec_commands);
}

header('Content-Type: application/json');
header('Cache-Control: no-store');

$status = Status::download_status($ip);
$response['errno'] = $status[0];
if (!$status[0]) {
    $response['status'] = $status[1];
}

echo json_encode($response);
?>
